require 'sinatra/base'

module SupportBot
  # Run sinatra main
  class Web < Sinatra::Base
    get '/' do
      'Nothing here :)'
    end
  end
end
