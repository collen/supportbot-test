require 'zendesk_api'
require 'json'

module ZendeskHelper
  require_relative '../services/zendesk'

  def self.count_many(zendesk_views)
    is_fresh = false
    while is_fresh == false
      fresh_arr = []
      output_hash = {}
      view_ids = zendesk_views.values.join(',')
      api_response = zd_client.view_counts(ids: view_ids, path: 'views/count_many', reload: true).fetch
      api_response.each do |json_views|
        fresh_arr.push(json_views['fresh'])
        output_hash[zendesk_views.key(json_views['view_id'])] = json_views['value']
      end
      is_fresh = (fresh_arr.uniq == [true])
    end
    output_hash
  end

  def self.tickets_from_views
    zendesk_views = {
      tier_1: 191_553_948,
      tier_2: 191_554_648,
      tier_3: 192_105_887,
      tier_1_open: 188_146_867,
      tier_2_open: 187_671_568,
      tier_3_open: 187_723_028,
      tier_1_breach: 157_094_588,
      tier_2_breach: 157_099_688,
      tier_3_breach: 187_726_508,
      tier_1_near: 191_619_388,
      tier_2_near: 192_179_807,
      tier_3_near: 191_620_768,
      twitter: 805_033_38,
      disqus: 634_215_17,
      community: 160_588_168,
      premium: 304_899_127,
      basic: 326_010_167,
      dot_com: 326_010_287,
      all: 326_010_407,
      # Revised views effective Jan 2018:
      important_tickets: 326_541_428,
      premium_gonna_breach: 325_771_188,
      starter_gonna_breach: 326_081_527,
      githost: 327_400_488,
      dot_com_customers: 328_714_287,
      all_dot_com: 306_398_488,
      all_on_prem: 198_393_487
    }

    ZendeskHelper.count_many(zendesk_views)
  end

  def self.get_ticket_emoji(total)
    case total
    when 0..22
      ':yay: :tada:'
    when 22..30
      ':white_check_mark: :white_check_mark:'
    when 30..45
      ':ok_hand: :pizza:'
    when 45..60
      ':fire:'
    when 60..70
      ':dumpster-fire: :fire: :fine:'
    else
      ':rocking:'
    end
  end
end
