# Support Bot [![Build status](https://gitlab.com/gl-support/gitlab-support-bot/badges/master/build.svg)](https://gitlab.com/gl-support/gitlab-support-bot/commits/master)

### Installation

```
git clone https://gitlab.com/gl-support/gitlab-support-bot.git
cd gitlab-support-bot
bundle install
```

### Deployment

#### Generate API tokens

**Slack**

+ https://gitlab.slack.com/apps/manage/custom-integrations
+ Click "Bots"
+ Click "Add Configuration"

**Zendesk**

Generate a new API token for Zendesk via https://gitlab.zendesk.com/agent/admin/api

#### Production deployment

The production deployment of support-bot is handled via terraform and docker.

1. Add the following variables under - https://gitlab.com/gitlab-com/dev-resources/settings/ci_cd
  + `TF_VAR_SUPPORT_BOT_SLACK`  = SLACK API TOKEN
  + `TF_VAR_SUPPORT_BOT_ZENDESK` = ZENDESK API TOKEN

2. See the deployment file https://gitlab.com/gitlab-com/dev-resources/blob/master/dev-resources/gitlab-supportbot.tf

The terraform template should be automatically build and deployed.

#### Manual deployment

```
docker run -d --env SLACK_API_TOKEN='$TOKEN' --env ZD_TOKEN='$TOKEN' "registry.gitlab.com/gl-support/gitlab-support-bot:latest"
```

#### Export API tokens

```
export SLACK_API_TOKEN=LONGTOKEN # Slack API token
export ZD_TOKEN=LONGTOKEN # Zendesk API token
```

## Commands

You would use any of the commands by calling `sb <command>` in Slack.  `sb` is
the name of the configured bot in Slack.

```
sb help
```

**Example Output**

```
+-----------------+----------------------------------------------------------------------+
|               SUPPORTBOT HELP (gitlab.com/gl-support/gitlab-support-bot)               |
+-----------------+----------------------------------------------------------------------+
| COMMAND(S)      | DESCRIPTION                                                          |
+-----------------+----------------------------------------------------------------------+
| help, h         | Learn the various commands available through `sb <command>`          |
+-----------------+----------------------------------------------------------------------+
| hi              | A warm greeting                                                      |
+-----------------+----------------------------------------------------------------------+
| pods, p         | Important, Premium Gonna Breach, EE Gonna Breach < 12 hrs, GitHost,  |
|                 | GitLab.com customers, All GitLab.com, and All on-prem queues         |
+-----------------+----------------------------------------------------------------------+
| self-hosted, sh | Important, Premium, and EE Gonna Breach in < 12 hours queues         |
+-----------------+----------------------------------------------------------------------+
| services, s     | GitHost, GitLab.com Customers, GitLab.com queues with the total      |
|                 | number of Services tickets                                           |
+-----------------+----------------------------------------------------------------------+
```

#### Development

Have (or create) a Slack bot for development, and a Zendesk token.

Create a `.env` file in the project directory and fill it with the appropriate
tokens for development:

```bash
ZD_TOKEN="xxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
SLACK_API_TOKEN="xxxxxxxxxxxxxxxxxxxxxxxxxxxx"
```

This file is *_not_* committed and is unique to your machine.

Run the server in a new tab with `foreman start`.  Then send this development bot a PM
in Slack to try out the commands (ex: `sb-testing tickets`).  Restart the
server when you make changes to the code.

## Troubleshooting

#### Supportbot is offline in Slack

If supportbot's presence in Slack shows as away and the bot is unresponsive:

- Rename the resource name for supportbot under [gitlab-supportbot.tf](https://gitlab.com/gitlab-com/dev-resources/blob/master/dev-resources/gitlab-supportbot.tf)
  - Example: "gitlab-supportbot-v2" => "gitlab-supportbot-v3" (do this on line 13 and line 40)
- Commit your changes
- Once the pipeline succeeds, merge your changes
  - This will destroy and rebuild supportbot based on the latest docker image
- Go back to Slack and check if supportbot is online
