require 'spec_helper'

describe SupportBot::Commands::Pods do
  # Record this once because the number of tickets will keep changing otherwise.
  vcr_options = {
    cassette_name: 'zendesk_tickets',
    record: :once,
    preserve_exact_body_bytes: true
  }

  def app
    SupportBot::Bot.instance
  end

  subject { app }

  it_behaves_like 'a slack ruby bot'

  context 'the commands', vcr: vcr_options do
    it 'returns pods tickets' do
      expect(message: "#{SlackRubyBot.config.user} pods", channel: 'channel').to respond_with_slack_message(expected_response)
    end

    it 'returns p tickets' do
      expect(message: "#{SlackRubyBot.config.user} p", channel: 'channel').to respond_with_slack_message(expected_response)
    end
  end

  def expected_response
    <<~RESPONSE
      Important: *0*
      Premium Gonna Breach: *2*
      EE Gonna Breach < 12 hrs: *9*
      GitHost: *8*
      GitLab.com customers: *10*
      All GitLab.com: *146*
      All on-prem: *62*
    RESPONSE
      .chomp
  end
end
