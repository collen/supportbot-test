require 'spec_helper'
require 'terminal-table'

describe SupportBot::Commands::Help do
  def app
    SupportBot::Bot.instance
  end

  subject { app }

  it_behaves_like 'a slack ruby bot'

  it 'returns as expected' do
    expect(message: "#{SlackRubyBot.config.user} help", channel: 'channel').to respond_with_slack_message(response_table)
    expect(message: "#{SlackRubyBot.config.user} h", channel: 'channel').to respond_with_slack_message(response_table)
  end

  # Test that ALL available commands show up with 'help'
  it 'contains all commands'

  private

  def response_table # rubocop:disable Metrics/LineLength
    table = Terminal::Table.new do |t|
      t.title = "SUPPORTBOT HELP (#{SupportBot::Commands::Help::PROJECT_LOCATION})"

      t.headings = ['COMMAND(S)', 'DESCRIPTION']
      t.add_row ['help, h', 'Learn the various commands available through `sb <command>`']
      t.add_row ['hi', 'A warm greeting']
      t.add_row ['pods, p',
                 "Important, Premium Gonna Breach, EE Gonna Breach < 12 hrs, GitHost, \nGitLab.com customers, All GitLab.com, and All on-prem queues"]
      t.add_row ['self-hosted, sh',
                 'Important, Premium, and EE Gonna Breach in < 12 hours queues']
      t.add_row ['services, s',
                 "GitHost, GitLab.com Customers, GitLab.com queues with the total \nnumber of Services tickets"]
      t.style = { all_separators: true, alignment: :left }
    end
    "```\n#{table}```"
  end
end
