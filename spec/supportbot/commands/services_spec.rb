require 'spec_helper'

describe SupportBot::Commands::Services do
  # Record this once because the number of tickets will keep changing otherwise.
  vcr_options = {
    cassette_name: 'zendesk_tickets',
    record: :once,
    preserve_exact_body_bytes: true
  }

  def app
    SupportBot::Bot.instance
  end

  subject { app }

  it_behaves_like 'a slack ruby bot'

  context 'the commands', vcr: vcr_options do
    it 'returns services tickets' do
      expect(message: "#{SlackRubyBot.config.user} services", channel: 'channel').to respond_with_slack_message(expected_response)
    end

    it 'returns s tickets' do
      expect(message: "#{SlackRubyBot.config.user} s", channel: 'channel').to respond_with_slack_message(expected_response)
    end
  end

  def expected_response
    <<~RESPONSE
      GitHost: *8*
      GitLab.com Customers: *10*
      GitLab.com: *146*
      All Services: *154* :rocking:
    RESPONSE
      .chomp
  end
end
