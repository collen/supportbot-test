require 'spec_helper'

describe SupportBot::Commands::SelfHosted do
  # Record this once because the number of tickets will keep changing otherwise.
  vcr_options = {
    cassette_name: 'zendesk_tickets',
    record: :once,
    preserve_exact_body_bytes: true
  }

  def app
    SupportBot::Bot.instance
  end

  subject { app }

  it_behaves_like 'a slack ruby bot'

  context 'the commands', vcr: vcr_options do
    it 'returns self-hosted tickets' do
      expect(message: "#{SlackRubyBot.config.user} self-hosted", channel: 'channel').to respond_with_slack_message(expected_response)
    end

    it 'returns sh tickets' do
      expect(message: "#{SlackRubyBot.config.user} sh", channel: 'channel').to respond_with_slack_message(expected_response)
    end
  end

  def expected_response
    <<~RESPONSE
      Important Tickets: *0*
      Premium Gonna Breach: *2*
      EE Gonna Breach < 12 hrs: *9*
      All self-hosted: *62* :dumpster-fire: :fire: :fine:
    RESPONSE
      .chomp
  end
end
