require 'spec_helper'

describe ZendeskHelper do
  vcr_search_options = {
    cassette_name: 'zendesk_search_api',
    record: :new_episodes,
    preserve_exact_body_bytes: true
  }

  vcr_view_options = {
    cassette_name: 'zendesk_view_api',
    record: :once,
    preserve_exact_body_bytes: true
  }

  def app
    ZendeskHelper
  end

  subject { app }

  context 'ticket counts from views', vcr: vcr_view_options # do
  #   it '#example_using_a_zd_view' do
  #     expect(app.example_using_a_zd_view).to eq(1)
  #   end
  # end

  context 'ticket counts', vcr: vcr_search_options # do
  #   it '#example_using_an_api_search' do
  #     expect(app.example_using_an_api_search).to eq(11)
  #   end
  # end
end
