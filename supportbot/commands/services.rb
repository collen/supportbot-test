require 'helpers/zendesk_helper'

module SupportBot
  module Commands
    class Services < SlackRubyBot::Commands::Base
      include ZendeskHelper
      command 'services', 's'
      def self.call(client, data, _match) # rubocop:disable Metrics/MethodLength
        tickets = ZendeskHelper.tickets_from_views
        if tickets
          all_services = tickets[:githost] + tickets[:all_dot_com]
          total = all_services
          response = "GitHost: *#{tickets[:githost]}*\n"\
                     "GitLab.com Customers: *#{tickets[:dot_com_customers]}*\n"\
                     "GitLab.com: *#{tickets[:all_dot_com]}*\n"\
                     "All Services: *#{all_services}* #{ZendeskHelper.get_ticket_emoji(total)}"
          client.say(channel: data.channel, text: response)
        else
          client.say(channel: data.channel, text: 'Failed to retrieve tickets :cry:')
        end
      rescue StandardError => e
        client.say(channel: data.channel, text: "Sorry, #{e.message}.")
      end
    end
  end
end
