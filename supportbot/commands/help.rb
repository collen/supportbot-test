require 'terminal-table'

module SupportBot
  module Commands
    class Help < SlackRubyBot::Commands::Base
      PROJECT_LOCATION = 'gitlab.com/gl-support/gitlab-support-bot'

      command 'help', 'h' do |client, data, _match|
        client.say(channel: data.channel, text: response_table)
      end

      def self.response_table
        table = Terminal::Table.new do |t|
          t.title = "SUPPORTBOT HELP (#{PROJECT_LOCATION})"
          t.headings = ['COMMAND(S)', 'DESCRIPTION']
          t.add_row ['help, h', 'Learn the various commands available through `sb <command>`']
          t.add_row ['hi', 'A warm greeting']
          t.add_row ['pods, p',
                     "Important, Premium Gonna Breach, EE Gonna Breach < 12 hrs, GitHost, \nGitLab.com customers, All GitLab.com, and All on-prem queues"]
          t.add_row ['self-hosted, sh',
                     'Important, Premium, and EE Gonna Breach in < 12 hours queues']
          t.add_row ['services, s',
                     "GitHost, GitLab.com Customers, GitLab.com queues with the total \nnumber of Services tickets"]
          t.style = { all_separators: true, alignment: :left }
        end

        "```\n#{table}```"
      end
    end
  end
end
