require 'helpers/zendesk_helper'

module SupportBot
  module Commands
    class SelfHosted < SlackRubyBot::Commands::Base
      include ZendeskHelper
      command 'self-hosted', 'sh'
      def self.call(client, data, _match)
        tickets = ZendeskHelper.tickets_from_views
        if tickets
          total = tickets[:all_on_prem]
          response = "Important Tickets: *#{tickets[:important_tickets]}*\n"\
                     "Premium Gonna Breach: *#{tickets[:premium_gonna_breach]}*\n"\
                     "EE Gonna Breach < 12 hrs: *#{tickets[:starter_gonna_breach]}*\n"\
                     "All self-hosted: *#{tickets[:all_on_prem]}* #{ZendeskHelper.get_ticket_emoji(total)}"
          client.say(channel: data.channel, text: response)
        else
          client.say(channel: data.channel, text: 'Failed to retrieve tickets :cry:')
        end
      rescue StandardError => e
        client.say(channel: data.channel, text: "Sorry, #{e.message}.")
      end
    end
  end
end
