require 'helpers/zendesk_helper'

module SupportBot
  module Commands
    class Pods < SlackRubyBot::Commands::Base
      include ZendeskHelper
      command 'pods', 'p'

      def self.call(client, data, _match)
        @tickets = nil

        if tickets
          client.say(channel: data.channel, text: response)
        else
          client.say(channel: data.channel, text: 'Failed to retrieve tickets :cry:')
        end
      rescue StandardError => e
        client.say(channel: data.channel, text: "Sorry, #{e.message}.")
      end

      def self.tickets
        @tickets ||= ZendeskHelper.tickets_from_views
      end

      def self.response
        "Important: *#{tickets[:important_tickets]}*\n"\
        "Premium Gonna Breach: *#{tickets[:premium_gonna_breach]}*\n"\
        "EE Gonna Breach < 12 hrs: *#{tickets[:starter_gonna_breach]}*\n"\
        "GitHost: *#{tickets[:githost]}*\n"\
        "GitLab.com customers: *#{tickets[:dot_com_customers]}*\n"\
        "All GitLab.com: *#{tickets[:all_dot_com]}*\n"\
        "All on-prem: *#{tickets[:all_on_prem]}*"
      end
    end
  end
end
