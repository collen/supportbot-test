############################################################
# Dockerfile build gitlab-support-bot
# https://gitlab.com/gl-support/gitlab-support-bot
############################################################
FROM ruby:2.3.1

MAINTAINER Collen Kriel, collen@gitlab.com

RUN apt-get update

RUN apt-get install -y git-core build-essential curl

RUN git clone "https://gitlab.com/collen/supportbot-test.git" /opt/support-bot-test

WORKDIR /opt/support-bot-test

RUN gem install bundler

RUN bundle install

HEALTHCHECK --interval=5m --timeout=3s --retries=3 \
  CMD curl -f localhost:5000 || exit 1

ENTRYPOINT ["bundle", "exec", "puma", "-p", "5000"]

