require 'zendesk_api'

def zd_client
  ZendeskApiClient.instance
end

class ZendeskApiClient < ZendeskAPI::Client
  def self.instance
    @instance ||= new do |config|
      config.url = 'https://gitlab.zendesk.com/api/v2'
      config.username = 'collen@gitlab.com/token'
      config.password = ENV['ZD_TOKEN']

      config.retry = true

      require 'logger'
      config.logger = Logger.new(STDOUT)
    end
  end
end
